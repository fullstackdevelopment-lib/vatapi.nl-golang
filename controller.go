package vatclient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"time"
)

// validate a vat number, landCode susch as "NL", vatNumber such as 0000000B01
func (c *VatClient) ValidateVAT(ctx context.Context, landCode, vatNumber string) (VatResponse, error) {
	var resp VatResponse

	requestBody, err := json.Marshal(map[string]string{
		"landCode":  landCode,
		"vatNumber": vatNumber,
	})

	if err != nil {
		return resp, err
	}

	request, _ := NewRequest(ctx, "POST", fmt.Sprintf("%s%s", c.urlBase(), "vatCheck"), bytes.NewReader(requestBody))

	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")

	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return resp, err
	}

	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, nil
}

// returns a list of al country codes.
func (c *VatClient) CountryCodes(ctx context.Context) ([]CountryCodeResponse, error) {
	var resp []CountryCodeResponse
	request, _ := NewRequest(ctx, "GET", fmt.Sprintf("%s%s", c.urlBase(), "countries/all"), nil)

	request.Header.Set("X-API-Key", c.Token)
	request.Header.Set("Content-type", "application/json")
	response, err := c.Do(ctx, request)

	if err != nil {
		return resp, err
	}

	bytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return resp, err
	}
	if err := json.Unmarshal(bytes, &resp); err != nil {
		return resp, err
	}
	defer func() {
		io.CopyN(ioutil.Discard, response.Body, 2)
		response.Body.Close()
	}()

	return resp, nil

}

type CountryCodeResponse struct {
	LandCode string
}

type VatResponse struct {
	CountryCode string
	VATnumber   string
	RequestDate time.Time
	Valid       bool
	Name        string
	Address     string
}
