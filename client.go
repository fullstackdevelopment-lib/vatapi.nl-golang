package vatclient

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"net/http"
	"time"
)

const DefaultBaseURL = "https://api.vatapi.nl/api/v1/public/vatapi/"

var (
	DefaultRetryWaitMin = 1 * time.Second
	DefaultRetryWaitMax = 30 * time.Second
	DefaultRetryMax     = 4
)

type VatClient struct {
	Token         string
	HTTPClient    *http.Client
	BaseURL       string
	RetryWaitMin  time.Duration
	RetryWaitMax  time.Duration
	RetryMax      int
	CheckForRetry CheckForRetry
	Backoff       Backoff
}

func NewClient(token string) *VatClient {
	return &VatClient{
		Token:         token,
		HTTPClient:    http.DefaultClient,
		RetryMax:      DefaultRetryMax,
		RetryWaitMin:  DefaultRetryWaitMin,
		RetryWaitMax:  DefaultRetryWaitMax,
		CheckForRetry: defaultRetryPolicy,
		Backoff:       defaultBackoff,
	}
}

func (c *VatClient) urlBase() string {
	if c.BaseURL == "" {
		return DefaultBaseURL
	}
	return c.BaseURL
}

type CheckForRetry func(resp *http.Response, err error) (bool, error)

func defaultRetryPolicy(resp *http.Response, err error) (bool, error) {
	if err != nil {
		return true, err
	}

	if resp.StatusCode == 0 || resp.StatusCode >= 500 {
		return true, nil
	}
	if resp.StatusCode == http.StatusTooManyRequests {
		return true, nil
	}
	return false, nil
}

type Backoff func(min, max time.Duration, attemptNum int, resp *http.Response) time.Duration

func defaultBackoff(min, max time.Duration, attemptNum int, resp *http.Response) time.Duration {
	mult := math.Pow(2, float64(attemptNum)) * float64(min)
	sleep := time.Duration(mult)
	if float64(sleep) != mult || sleep > max {
		sleep = max
	}
	return sleep
}

type Request struct {
	body io.ReadSeeker
	*http.Request
}

func NewRequest(ctx context.Context, method, url string, body io.ReadSeeker) (*Request, error) {
	var rcBody io.ReadCloser
	if body != nil {
		rcBody = ioutil.NopCloser(body)
	}

	httpReq, err := http.NewRequestWithContext(ctx, method, url, rcBody)
	if err != nil {
		return nil, err
	}

	return &Request{body, httpReq}, nil
}

func (c *VatClient) Do(ctx context.Context, req *Request) (*http.Response, error) {
	for {
		var code int
		if req.body != nil {
			if _, err := req.body.Seek(0, 0); err != nil {
				return nil, fmt.Errorf("failed to seek body: %v", err)
			}
		}

		resp, err := c.HTTPClient.Do(req.Request)
		if err != nil {
			fmt.Printf("ERR %s %s request failed: %v \n", req.Method, req.URL, err)
			return nil, err
		} else {
			// LOGGING ?
		}
		if resp.StatusCode == http.StatusUnauthorized {
			return nil, fmt.Errorf("Your token is not valid")
		}

		checkOK, checkErr := c.CheckForRetry(resp, err)
		
		if !checkOK {
			if checkErr != nil {
				err = checkErr
			}
			return resp, err
		}
		remain := c.RetryMax - 1
		if remain == 0 {
			break
		}
		wait := c.Backoff(c.RetryWaitMin, c.RetryWaitMax, 1, resp)
		desc := fmt.Sprintf("%s %s", req.Method, req.URL)
		if code > 0 {
			desc = fmt.Sprintf("%s status: %d", desc, code)
		}
		time.Sleep(wait)

	}

	return nil, fmt.Errorf("%s %s giving up after %d attempts", req.Method, req.URL, c.RetryMax+1)
}
